package com.danylko.view;

public interface Printable {
    void print();
}
