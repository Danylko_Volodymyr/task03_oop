package com.danylko.controller;


public interface Controller {
    void getAppliancesList();

    void sortByPower();

    void findAppliance(String brand, int lowerPriceRange, int topPriceRange,
                       int lowerPowerRange, int topPowerRange);

    int getPowerConsumption();
}
