package com.danylko.controller;


import com.danylko.model.Logic;
import com.danylko.model.Model;


public class MyController implements Controller {
    private Model model;

    public MyController() {
        model = new Logic();
    }

    @Override
    public void getAppliancesList() {
        model.getAppliancesList();
    }

    @Override
    public void sortByPower() {
        model.sortByPower();
    }

    @Override
    public void findAppliance(String brand, int lowerPriceRange, int topPriceRange,
                              int lowerPowerRange, int topPowerRange) {
        model.findAppliance(brand, lowerPriceRange,
                topPriceRange, lowerPowerRange, topPowerRange);
    }

    @Override
    public int getPowerConsumption() {
        return model.getPowerConsumption();
    }
}

