package com.danylko.model.appliances;

public abstract class Appliances implements Comparable {

    private String name;
    private String manufacturer;
    private int power;
    private int cost;
    private boolean switched = false;


    public Appliances(String name, String manufacturer, int power, int cost,
                      boolean switched) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.power = power;
        this.cost = cost;
        this.switched = switched;
    }

    public void switchOn() {
        this.switched = true;
    }

    public void switchOff() {
        this.switched = false;
    }

    public boolean isSwitched() {
        return switched;
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getPower() {
        return power;
    }

    public int getCost() {
        return cost;
    }

    @Override
    public int compareTo(Object o) {
        return ((Appliances) o).getPower() - this.getPower();
    }

    public void display() {
        System.out.printf("Name: %s %n Brand: %s %n Power: %d %n Cost: %d %n Is switched: %b %n",
                getName(), getManufacturer(), getPower(), getCost(), isSwitched());
    }
}
