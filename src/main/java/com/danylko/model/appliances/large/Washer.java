package com.danylko.model.appliances.large;

import com.danylko.model.appliances.Appliances;

public class Washer extends Appliances {
    private int maxLoad;

    public Washer(String name, String manufacturer, int power, int cost,
                  boolean switched, int maxLoad) {
        super(name, manufacturer, power, cost, switched);
        this.maxLoad = maxLoad;
    }

    public int getMaxLoad() {
        return maxLoad;
    }

    @Override
    public void display() {
        super.display();
        System.out.printf("Type %s %n", getMaxLoad());
    }
}

