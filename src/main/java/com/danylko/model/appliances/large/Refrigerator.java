package com.danylko.model.appliances.large;

import com.danylko.model.appliances.Appliances;

public class Refrigerator extends Appliances {


    private String type;


    public Refrigerator(String name, String manufacturer, int power, int cost,
                        boolean switched, String type) {
        super(name, manufacturer, power, cost, switched);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public void display() {
        super.display();
        System.out.printf("Type %s %n", getType());
    }

}

