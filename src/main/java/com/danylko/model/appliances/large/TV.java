package com.danylko.model.appliances.large;

import com.danylko.model.appliances.Appliances;

public class TV extends Appliances {

    private int diagonal;

    public TV(String name, String manufacturer, int power, int cost,
              boolean switched, int diagonal) {
        super(name, manufacturer, power, cost, switched);
        this.diagonal = diagonal;
    }

    public int getDiagonal() {
        return diagonal;
    }

    @Override
    public void display() {
        super.display();
        System.out.printf("Diagonal %d %n", getDiagonal());
    }
}

