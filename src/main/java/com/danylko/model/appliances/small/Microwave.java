package com.danylko.model.appliances.small;

import com.danylko.model.appliances.Appliances;

public class Microwave extends Appliances {
    private boolean isGrill;

    public Microwave(String name, String manufacturer, int power, int cost,
                     boolean switched, boolean isGrill) {
        super(name, manufacturer, power, cost, switched);
        this.isGrill = isGrill;
    }

    public boolean isGrill() {
        return isGrill;
    }

    @Override
    public void display() {
        super.display();
        System.out.printf("Is grill %b %n", isGrill());
    }
}

