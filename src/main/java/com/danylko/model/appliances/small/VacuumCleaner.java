package com.danylko.model.appliances.small;

import com.danylko.model.appliances.Appliances;

public class VacuumCleaner extends Appliances {
    private String type;

    public VacuumCleaner(String name, String manufacturer, int power, int cost,
                         boolean switched, String type) {
        super(name, manufacturer, power, cost, switched);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public void display() {
        super.display();
        System.out.printf("Type %s %n", getType());
    }
}

