package com.danylko.model.appliances.small;


import com.danylko.model.appliances.Appliances;

public class ElectricKettle extends Appliances {
    private String material;

    public ElectricKettle(String name, String manufacturer, int power, int cost,
                          boolean switched, String material) {
        super(name, manufacturer, power, cost, switched);
        this.material = material;
    }


    public String getMaterial() {
        return material;
    }

    @Override
    public void display() {
        super.display();
        System.out.printf("Material %s %n", getMaterial());
    }
}

