package com.danylko.model;

public interface Model {
    void getAppliancesList();

    void sortByPower();

    void findAppliance(String brand, int lowerPriceRange, int topPriceRange,
                       int lowerPowerRange, int topPowerRange);

    int getPowerConsumption();


}
