package com.danylko.model;

public class Logic implements Model {
    private MyAppliances myAppliances;

    public Logic() {
        this.myAppliances = new MyAppliances();
    }

    @Override
    public void getAppliancesList() {
        myAppliances.getList();
    }

    @Override
    public void sortByPower() {
        myAppliances.sortByPower();
    }

    @Override
    public void findAppliance(String brand, int lowerPriceRange, int topPriceRange,
                              int lowerPowerRange, int topPowerRange) {
        myAppliances.findAppliance(brand, lowerPriceRange, topPriceRange,
                lowerPowerRange, topPowerRange);
    }

    @Override
    public int getPowerConsumption() {
        return myAppliances.getPowerConsumption();
    }
}
