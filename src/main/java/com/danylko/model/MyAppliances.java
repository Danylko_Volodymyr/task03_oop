package com.danylko.model;


import com.danylko.model.appliances.Appliances;
import com.danylko.model.appliances.large.Refrigerator;
import com.danylko.model.appliances.large.TV;
import com.danylko.model.appliances.large.Washer;
import com.danylko.model.appliances.small.ElectricKettle;
import com.danylko.model.appliances.small.Microwave;
import com.danylko.model.appliances.small.VacuumCleaner;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MyAppliances {
    private List<Appliances> list;
    private int powerConsumption;

    public MyAppliances() {
        list = new LinkedList<Appliances>();
        list.add(new TV(
                "TV",
                "LG",
                500,
                15000,
                true,
                6));
        list.add(new Microwave(
                "Microwave",
                "LG",
                900,
                1500,
                true,
                false));
        list.add(new Refrigerator(
                "Refrigerator",
                "LG",
                300,
                20000,
                true,
                "Two cameras"));
        list.add(new ElectricKettle(
                "Electric Kettle",
                "LG",
                2000,
                550,
                false,
                "df"));
        list.add(new VacuumCleaner(
                "Vacuum Cleaner",
                "LG",
                2000,
                2000,
                false,
                "df"));
        list.add(new Washer(
                "Washer",
                "Ardo",
                1800,
                12000,
                false,
                5));
        powerConsumption = 0;
    }

    public void getList() {
        for (Appliances item : list
        ) {
            item.display();
            System.out.println();
        }
    }

    public int getPowerConsumption() {
        for (Appliances item : list
        ) {
            if (item.isSwitched() == true) {
                powerConsumption += item.getPower();
            }
        }
        return powerConsumption;
    }

    public void sortByPower() {
        Collections.sort(list);
    }

    public void findAppliance(String brand, int lowerPriceRange, int topPriceRange,
                              int lowerPowerRange, int topPowerRange) {
        List<Appliances> tmp = new LinkedList<>();
        for (Appliances item : list
        ) {
            if (item.getManufacturer().equals(brand) &&
                    item.getCost() > lowerPriceRange &&
                    item.getCost() < topPriceRange &&
                    item.getPower() > lowerPowerRange &&
                    item.getPower() < topPowerRange) {
                tmp.add(item);
            }
        }
        if (tmp.isEmpty()) {
            System.out.println("There is no appliance with such parameters");
        } else {
            for (Appliances item : tmp
            ) {
                item.display();
            }
        }
    }
}
